import React, { useState } from "react";
import axios from 'axios';
import { LineChart, Line, CartesianGrid, YAxis, XAxis } from "recharts"
import "../css/weather.css"


const weather = props => {
  const [weatherData, setWeatherData] = useState();
  const {userSigned} = props;

    const handleSubmit = async (event) => {
    event.preventDefault();

    const weather = await axios.get('http://localhost:8000/getweather')
    .then(res => {
      return res.data.data;
    });
    setWeatherData(weather)
 };


  const renderWeather = (
    <div>
      <div className="userConnect">
        <h3>{userSigned}</h3>
      </div>
      <div className="fetchDataButton">
        <button onClick={handleSubmit}>Fetch Data</button>
      </div>
      <div className="table">
        <LineChart width={800} height={300} data={weatherData}>
          <Line type="monotone" dataKey="temperature" stroke="green" dot={false}/>
          <CartesianGrid stroke="gray" />
          <YAxis />
          <XAxis dataKey="time" fontSize={10}/>
        </LineChart>
      </div>
    </div>     
  );

  return (
    <div className="weather">
        {renderWeather}      
    </div>
  );
}

export default weather;

