import Form from "./components/form";
import Weather from "./components/weather"
import React, { useState, useEffect } from 'react';

const App = () => {
  const [userSigned, setUser] = useState("");

  const handleUser = (user) => {
    setUser(user);
  }

  useEffect(() => {
    setUser(userSigned)
}, [])

  return (
    <div ClassName="App">
      {userSigned != "" ? <Weather userSigned={userSigned}/> : 
      <Form userSigned={userSigned} onUserSigned={handleUser}/>}
    </div>
  )
}

export default App;