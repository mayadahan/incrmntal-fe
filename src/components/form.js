import React, { useState } from "react";
import "../css/form.css";
import axios from 'axios';

const form = props =>{
  const [errorMessages, setErrorMessages] = useState({});
  const {onUserSigned} = props;

  const errors = {
    email: "invalid email",
    pass: "invalid password"
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    let { email, pass } = document.forms[0];
    
    const userData = await axios.get('http://localhost:8000/getusers', {
    params: {
      email: email.value,
      password: pass.value
    }}).then(res => {
      return res.data.data });

    if (userData.res === "ok")
      onUserSigned(userData.email);
    else if (userData.res === "pass")
        setErrorMessages({ name: "pass", message: errors.pass });
    else
      setErrorMessages({ name: "email", message: errors.email });
  };

  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div>
          <h2>Sign In</h2>
        </div>
        <div>
          <label>Username</label>
          <input type="text" name="email" required />
          {renderErrorMessage("email")}
        </div>
        <div>
          <label>Password </label>
          <input type="password" name="pass" required />
          {renderErrorMessage("pass")}
        </div>
        <div className="button-container">
          <input type="submit" />
        </div>
      </form>
    </div>
  );

  return (
    <div className="form">
      <div className="login-form">
        {renderForm}
      </div>
    </div>
  );
}

export default form;